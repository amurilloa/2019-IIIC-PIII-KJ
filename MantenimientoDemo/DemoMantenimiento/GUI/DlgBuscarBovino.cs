﻿using DemoMantenimiento.BOL;
using MantenimientoDemo.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoMantenimiento.GUI
{
    public partial class DlgBuscarBovino : Form
    {
        private BovinoBOL bovBOL;
        public EBovino Bovino { get; set; }

        public DlgBuscarBovino()
        {
            InitializeComponent();
            bovBOL = new BovinoBOL();
        }

        private void btnTrazabilidad_Click(object sender, EventArgs e)
        {
            listBox1.DataSource = bovBOL.CargarBovinos(txtFiltro.Text.Trim());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bovino = listBox1.SelectedItem as EBovino;
            if (Bovino != null)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Bovino = null;
            DialogResult = DialogResult.Cancel;
        }
    }
}
