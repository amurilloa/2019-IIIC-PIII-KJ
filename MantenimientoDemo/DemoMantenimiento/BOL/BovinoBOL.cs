﻿using DemoMantenimiento.DAL;
using MantenimientoDemo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoMantenimiento.BOL
{
    public class BovinoBOL
    {
        public List<EBovino> CargarBovinos(string filtro)
        {
            return new BovinoDAL().CargarBovinos(filtro);
        }

        public void guardar(EBovino b)
        {
            validar(b);
            if (b.Id <= 0)
            {
                new BovinoDAL().Insertar(b);
            }
            else { 
                new BovinoDAL().Modificar(b);
            }
        }

        private void validar(EBovino b)
        {
            if (b == null)
            {
                throw new Exception("Bovino inválido");
            }

            if (b.Trazabilidad?.Id == b.Id) 
            {
                throw new Exception("Favor seleccionar correctamente la trazabilidad");
            }

            if (b.Estado == null || b.Genero == null || b.Ganaderia == null) { 
                throw new Exception("Datos requeridos, favor intente nuevamente");

            }
        }

        public EBovino CargarBovino(int id)
        {
            return new BovinoDAL().CargarBovinos(id);
        }
    }
}
