﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MantenimientoDemo.Entities;
using Npgsql;

namespace DemoMantenimiento.DAL
{
    public class GanaderiaDAL
    {
        public EGanaderia CargarGanaderia(int id)
        {
            string sql = "select id, nombre, id_imagen, activo from ganaderia.ganaderias where id = @id";

            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);
                
                NpgsqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    return CargarGanadería(reader);
                }
            }
            return null;

        }

        public List<EGanaderia> CargarTodo()
        {

            List<EGanaderia> ganaderias = new List<EGanaderia>();

           string sql = "select id, nombre, id_imagen, activo from ganaderia.ganaderias where activo = true";

            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ganaderias.Add(CargarGanadería(reader));
                }
            }
            return ganaderias ;
        }

        private EGanaderia CargarGanadería(NpgsqlDataReader reader)
        {
            EGanaderia g = new EGanaderia
            {
                Id = reader.GetInt32(0),
                Nombre = reader.GetString(1),
                Sello = null
            };
            return g;   
        }
    }
}
