﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MantenimientoDemo.Entities;
using Npgsql;

namespace DemoMantenimiento.DAL
{
    class GeneroDAL
    {
        public EGenero CargarGenero(int id)
        {
            string sql = "select id, genero, activo, simbolo from ganaderia.generos where id = @id";

            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    return CargarGenero(reader);
                }
            }
            return null;

        }

        public List<EGenero> CargarTodo()
        {

            List<EGenero> generos = new List<EGenero>();

            string sql = "select id, genero, activo, simbolo from ganaderia.generos where activo = true";

            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    generos.Add(CargarGenero(reader));
                }
            }
            return generos;
        }

        private EGenero CargarGenero(NpgsqlDataReader reader)
        {
            EGenero g = new EGenero
            {
                Id = reader.GetInt32(0),
                Genero = reader.GetString(1),
                Activo = reader.GetBoolean(2),
                Simbolo = reader.GetChar(3)
            };
            return g;
        }

    }
}
