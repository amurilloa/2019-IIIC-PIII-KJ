﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MantenimientoDemo.Entities;
using Npgsql;

namespace DemoMantenimiento.DAL
{
    class ImagenDAL
    {
        public EImagen CargarImagen(int id)
        {
            try
            {
                string sql = "select  id, imagen, activo from ganaderia.imagenes where id = @id";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@id", id);
                    NpgsqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        return CargarImagen(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        private EImagen CargarImagen(NpgsqlDataReader reader)
        {
            EImagen img = new EImagen
            {
                Id = reader.GetInt32(0),
                Imagen = reader[1] != DBNull.Value ? Image.FromStream(new MemoryStream((byte[])reader[1])) : null,
                Activo = reader.GetBoolean(2)
            };
            return img;
        }

        public int InsertarImagen(EImagen imagen, NpgsqlConnection con, NpgsqlTransaction tran)
        {
            try
            {
                string sql = "INSERT INTO ganaderia.imagenes(imagen) VALUES(@ima) returning id; ";

                NpgsqlCommand cmd = new NpgsqlCommand(sql, con, tran);
                MemoryStream ms = new MemoryStream();
                imagen.Imagen.Save(ms, ImageFormat.Jpeg);
                cmd.Parameters.AddWithValue("@ima", ms.ToArray());
                int id = (int)cmd.ExecuteScalar();
                return id;
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int InsertarImagen(EImagen imagen)
        {
            try
            {
                string sql = "INSERT INTO ganaderia.imagenes(imagen) VALUES(@ima) returning id; ";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    MemoryStream ms = new MemoryStream();
                    imagen.Imagen.Save(ms, ImageFormat.Jpeg);
                    cmd.Parameters.AddWithValue("@ima", ms.ToArray());
                    int id = (int)cmd.ExecuteScalar();
                    return id;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
