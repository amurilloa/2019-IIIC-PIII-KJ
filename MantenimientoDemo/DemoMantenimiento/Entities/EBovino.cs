﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MantenimientoDemo.Entities
{
    public class EBovino
    {
        public int Id { get; set; }
        public int Numero { get; set; }
        public EBovino Trazabilidad { get; set; }
        public EGanaderia Ganaderia { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public EImagen Foto { get; set; }
        public EGenero Genero { get; set; }
        public EEstado Estado { get; set; }
        public bool Activo { get; set; }

        public String Texto
        {
            get { return String.Format("{0} - {1}({2})", Numero, Genero.Genero, Estado.Estado); }
        }

        public String FechaTxT
        {
            get { return FechaNacimiento.ToString("dd/MM/yyyy"); }
        }
        public override string ToString()
        {
            return String.Format("{0} - {1}({2})", Numero, Genero.Genero, Estado.Estado);
        }

    }
}
