CREATE SCHEMA ganaderia AUTHORIZATION postgres;


CREATE TABLE ganaderia.imagenes(
	id serial primary key,
	imagen bytea,
	activo boolean default true
);

CREATE TABLE ganaderia.ganaderias(
	id serial primary key,
	nombre text not null unique,
	id_imagen int not null, 
	activo boolean default true,
	CONSTRAINT fk_gan_img FOREIGN KEY (id_imagen)
        REFERENCES ganaderia.imagenes (id)
);

CREATE TABLE ganaderia.generos(
	id serial primary key,
	genero text not null, 
	simbolo char not null,
	activo boolean default true
)

CREATE TABLE ganaderia.estados(
	id serial primary key,
	estado text not null, 
	id_genero int not null,
	activo boolean default true,
	CONSTRAINT fk_est_gen FOREIGN KEY (id_genero)
        REFERENCES ganaderia.generos(id)
)

CREATE TABLE ganaderia.bovinos(
	id serial primary key, 
	numero int not null unique, 
	id_bovino int, --id de referencia a la vaca mamá
	id_ganaderia int not null,
	fecha_nacimiento timestamp not null,
	id_imagen int, 
	id_genero int not null,
	id_estado int not null,
	activo bool default true,
	CONSTRAINT fk_bov_bov FOREIGN KEY (id_bovino)
        REFERENCES ganaderia.bovinos(id), 
	CONSTRAINT fk_bov_gan FOREIGN KEY (id_ganaderia)
        REFERENCES ganaderia.ganaderias(id), 
	CONSTRAINT fk_bov_img FOREIGN KEY (id_imagen)
        REFERENCES ganaderia.imagenes(id), 
	CONSTRAINT fk_bov_gen FOREIGN KEY (id_genero)
        REFERENCES ganaderia.generos(id), 
	CONSTRAINT fk_bov_est FOREIGN KEY (id_estado)
        REFERENCES ganaderia.estados(id)
);
