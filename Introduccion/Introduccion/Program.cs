﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Introduccion
{
    class Program
    {
        static int LeerInt(string msj)
        {
            Console.Write(msj + " ");
            int op = Int32.Parse(Console.ReadLine());
            return op;
        }

        static void Main(string[] args)
        {
            string menu = "Práctica 1 - UTN\n" +
                "1. Sumar\n" +
                "2. Restar\n" +
                "3. Multiplicación\n" +
                "4. División\n" +
                "5. Arreglo\n" +
                "6. Imprimir Arreglo\n" +
                "7. Salir";

            Operaciones ope = new Operaciones();

            while (true)
            {
                Console.WriteLine(menu);
                Console.Write("Seleccione una opción: ");
                int op = Int32.Parse(Console.ReadLine());
                Console.Clear();
                if (op >= 1 && op <= 4)
                {
                    int n1 = LeerInt("#1:");
                    int n2 = LeerInt("#2:");
                    string o = op == 1 ? "+" : op == 2 ? "-" : op == 3 ? "*" : "/";
                    double result = ope.Calcular(n1, n2, op);
                    string res = String.Format("\n>> {0}{1}{2}={3}\n", n1, o, n2, Math.Round(result, 2));
                    Console.WriteLine(res);
                }
                else if (op == 5)
                {
                    int dim = LeerInt("Dimensión:");
                    ope.LlenarArreglo(dim);
                }
                else if (op == 6)
                {
                    Console.WriteLine(ope.ImprimirArreglo());
                }
                else if (op == 7)
                {
                    goto SALIDA;
                }
            }
        SALIDA:
            Console.WriteLine("Gracias por utilizar la aplicación!!");
            Console.ReadKey();
        }



    }
}
