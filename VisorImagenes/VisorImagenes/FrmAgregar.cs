﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisorImagenes.BOL;
using VisorImagenes.Entities;

namespace VisorImagenes
{
    public partial class FrmAgregar : Form
    {
        private ImagenBOL ibol;
        public EImagen Imagen { get; set; }

        public FrmAgregar()
        {
            InitializeComponent();
            ibol = new ImagenBOL();
        }

        private void pbxFoto_Click(object sender, EventArgs e)
        {
            DialogResult res = openFileDialog1.ShowDialog();
            if (res == DialogResult.OK)
            {
                pbxFoto.Image = Image.FromFile(openFileDialog1.FileName);
            }
            else
            {
                pbxFoto.Image = null;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            EImagen img = new EImagen
            {
                Id = Imagen != null ? Imagen.Id : 0,
                Titulo = txtTitulo.Text.Trim(),
                Descripcion = txtDescripcion.Text.Trim(),
                Tipo = cbxTipo.Text.Trim(),
                Imagen = pbxFoto.Image
            };
            ibol.Guardar(img);
            DialogResult = DialogResult.OK;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void FrmAgregar_Load(object sender, EventArgs e)
        {
            cbxTipo.DataSource = ibol.CargarTipos();
            if (Imagen != null)
            {
                txtTitulo.Text = Imagen.Titulo;
                txtDescripcion.Text = Imagen.Descripcion;
                cbxTipo.SelectedItem = Imagen.Tipo;
                pbxFoto.Image = Imagen.Imagen;
            }
        }
    }
}
