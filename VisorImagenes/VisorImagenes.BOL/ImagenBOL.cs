﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisorImagenes.DAL;
using VisorImagenes.Entities;

namespace VisorImagenes.BOL
{
    public class ImagenBOL
    {
        public List<EImagen> Cagar()
        {
            return new ImagenDAL().Cargar();
        }
        public void Guardar(EImagen img)
        {
            if (img.Id > 0)
            {
                new ImagenDAL().Actualizar(img);
            }
            else
            {
                new ImagenDAL().InsertarImagen(img);
            }
        }

        public List<String> CargarTipos()
        {
            return new ImagenDAL().CargarTipos();
        }

        public void Eliminar(EImagen img)
        {

            if (img.Id > 0)
            {
                new ImagenDAL().EliminarImagen(img);

            }
        }
    }
}
