﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerInterfazParteDos.IngresarDatos
{
    public partial class FrmIngresarDatos : Form
    {
        public FrmIngresarDatos()
        {
            InitializeComponent();
        }

        public string Nombre
        {
            set { lblNombre.Text = value; }
        }

        public string Correo
        {
            get { return txtCorreo.Text.Trim(); }
        }

        public string Telefono
        {
            get { return txtTele.Text.Trim(); }
        }

        public string Provincia
        {
            get { return txtProvincia.Text.Trim(); }
        }

        public string Canton
        {
            get { return txtCanton.Text.Trim(); }
        }

        public string Distrito
        {
            get { return txtDistrito.Text.Trim(); }
        }
        public int CodigoPostal
        {
            get { return Convert.ToInt32(txtCodigo.Text.Trim()); }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
