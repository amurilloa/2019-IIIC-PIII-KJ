﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TareaUno.EjercicioDiez
{
    class Producto
    {
        public String Nombre { get; set; }
        public int Precio { get; set; }

        private int cantidad;
        public int Cantidad
        {
            get { return cantidad; }
            set { cantidad = value < 0 ? 0 : value; }
        }

        public override string ToString()
        {
            string can = Cantidad == 0 ? "" : String.Format(" =>{0}", Cantidad.ToString().PadLeft(2, ' '));
            return String.Format("{0}(${1}){2}", Nombre.PadRight(25, '.'), Precio.ToString().PadLeft(3, ' '), can);
        }

    }
}
