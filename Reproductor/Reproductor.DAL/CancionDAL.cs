﻿using Npgsql;
using Reproductor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reproductor.DAL
{
    public class CancionDAL
    {
        public void Insertar(ECancion cancion)
        {
            string sql = "insert into reproductor.canciones(numero, cancion, album, artista,categoria, datos) " +
                " values(@num, @can, @alb, @art,@cat, @dat)";

            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@num", cancion.Numero);
                cmd.Parameters.AddWithValue("@can", cancion.Cancion);
                cmd.Parameters.AddWithValue("@alb", cancion.Album);
                cmd.Parameters.AddWithValue("@art", cancion.Artista);
                cmd.Parameters.AddWithValue("@cat", cancion.Categoria);
                cmd.Parameters.AddWithValue("@dat", cancion.Datos);

                cmd.ExecuteNonQuery();
            }
        }

        public List<ECancion> Cargar(ECancion filtro)
        {
            List<ECancion> canciones = new List<ECancion>();
            string sql = "select  id, numero, cancion, album, artista,categoria, datos from reproductor.canciones" +
                " order by categoria, artista, album, numero";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                NpgsqlDataReader reader =  cmd.ExecuteReader();
                while (reader.Read()) {
                    canciones.Add(CargarCancion(reader));
                }
            }
            return canciones;
        }

        private ECancion CargarCancion(NpgsqlDataReader reader)
        {
            ECancion can = new ECancion
            {
                Id = reader.GetInt32(0),
                Numero = reader.GetInt32(1),
                Cancion = reader.GetString(2),
                Album = reader.GetString(3),
                Artista = reader.GetString(4),
                Categoria = reader.GetString(5),
                Datos = (byte[])reader[6]
            };
            return can;
        }
    }
}
