﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reproductor.Entities
{
    public class ECancion
    {
        public int Id { get; set; }
        public int Numero { get; set; }
        public string Cancion { get; set; }
        public string Album { get; set; }
        public string Artista { get; set; }
        public string Categoria { get; set; }
        public byte[] Datos { get; set; }

        public override string ToString()
        {
            return Cancion;
        }
    }
}
