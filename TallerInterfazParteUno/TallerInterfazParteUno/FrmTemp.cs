﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerInterfazParteUno
{
    public partial class FrmTemp : Form
    {
        public FrmTemp()
        {
            InitializeComponent();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            txtRes.Clear();
            txtTemp.Clear();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            int c = Convert.ToInt32(txtTemp.Text);
            double f = c / (5.0 / 9) + 32;
            txtRes.Text = String.Format("{0:F3}", f);
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            int f = Convert.ToInt32(txtTemp.Text);
            double c = (f - 32) * 5 / 9;
            txtRes.Text = String.Format("{0:F3}", c);
        }
    }
}
